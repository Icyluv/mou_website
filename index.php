<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="components/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="components/css/stylesheet.css"/>
    <title>Its_A_Start</title>
</head>
<body>
<?php  include_once('components/includes/header.php');?>
<div class=".container-fluid">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="components/images/y9.jpeg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="components/images/y16.jpeg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="components/images/y3.jpeg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div class="container-fluid lwanga lightgreyBg" >
    <div class="row">
        <div class="col-md-3 imgContainer">
            <div class="MOU">
                <img src="components/images/MOU_Building.jpeg"/>
            </div>
        </div>
        <div class="col-md-9">
            <h1>Theme For The Year</h1>
            <h3>Be faithful until death, and I will give you the crown of life Rev 2:10b</h3>
            <p>Coming Sunday we will raise funds representing our schools in the various regions. Please do well to get
                anything of your school be it School Uniform, School polo shirt, Tie, Cloth etc just to motivate our
                youth yet to go to school to familiarize with parishioners who attended this school or the other.
                Also do try and contribute an amount of Gh100.00 or 50.00 as your personal contribution towards this
                course.
            </p>
        </div>
    </div>
</div>

<div class="container">
    <div class="row home_images homeTheme">
        <div class="col-md-8">
            <h4 class="redColour">Church Activities</h4><hr/>
            <div class="row home_images">
            <?php
            $imgs=array("y10.jpeg","y10.jpeg","y10.jpeg","y10.jpeg");
            for($i=0;$i<4;$i++){?>
                <div class="col-md-6 imgContainer lwanga">
                    <div class="homeimg">
                        <img src="components/images/<?php echo $imgs[$i];?>" width="100%">
                    </div>
                    <div>
                        <h4>Martyrs Youth</h4>
                        <p>
                            Power in Action. We are the future leaders of our great parish and we will contribute in our
                            diverse ways  ...
                        </p>
                    </div>
                </div>
            <?php }?>
            </div>
        </div>
        <div class="col-md-4">
            <h4 class="redColour">Church News</h4><hr/>
                <?php
                $imgs=array("y10.jpeg","y10.jpeg","y10.jpeg","y10.jpeg");
                for($i=0;$i<4;$i++){?>
                 <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-11">
                        <div class="row home_images space40">
                            <div class="col-md-5">
                                <div class="smallimgBox">
                                    <img src="components/images/<?php echo $imgs[$i];?>" width="100%">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <article class=" b-line">
                                <small><b class="redColour">5th JUNE, 2017</b></small>
                                <p>
                                    <a href="" class="grayColour">Power in Action. We are the future leaders of our great parish  ...</a>
                                </p>
                                </article>
                            </div>
                        </div>
                    </div>
                 </div>
                <?php }?>

        </div>
    </div>
</div>
<?php  include_once('components/includes/footer.php');?>


<script src="components/js/jquery-3.2.1.slim.min.js"></script>
<script src="components/js/popper.min.js"></script>
<script src="components/js/bootstrap.min.js"></script>
</body>
