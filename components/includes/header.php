<?php
/**
 * Created by IntelliJ IDEA.
 * User: ICY
 * Date: 11/11/2017
 * Time: 18:46
 */?>
<div class="container-fluid" id="hmeHeader">
    <div class="row">
        <div class="col-md-3 r-align">
            <div id="Home_header" class="menuFont">
                <img id="imgHeader" class="c-align" src="components/images/mlogo.JPG"/>
            </div>
        </div>
        <div class="col-md-6">
            <div id="#txtHeader">
                <h1 class="menuFont l-align">Martyrs Of Uganda Catholic Church</h1>
                <p class="menuFont">Sacrifice and Love, Pray For Us</p>
            </div>
        </div>
        <div class="col-md-3 l-align">
            <div id="Home_header" class="menuFont">
                <img id="imgHeader" class="c-align" src="components/images/Coat_of_arms_of_the_Vatican_City.JPG"/>
            </div>
        </div>
    </div>
</div>
<div id="navMenu" >
    <nav class="navbar navbar-expand-lg navbar-light navbar-default navbar-fixed-top">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="navbar-collapse navlist">
            <ul class="nav navbar-nav">
                <li class="nav-item active ">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="Teaching.php">Teachings <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="YouthCorner.php">Youth</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link " href="#">School</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link " href="Gallery.php">Gallery</a>
                </li>
            </ul>
        </div>
    </nav>
</div>


