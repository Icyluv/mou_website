<?php
/**
 * Created by IntelliJ IDEA.
 * User: ICY
 * Date: 11/11/2017
 * Time: 19:41
 */
?>
<div class="hmefooter">
    <div class="container-fluid">
        <div class="row hometext">
            <div class="col-md-4">
                <h4>Mass Schedules</h4>
                <p>
                    <strong>Sunday</strong> : First Mass - 7am , Second Mass - 9am
                </p>
                <p>
                    <strong>Weekday</strong> : <strong>Morning</strong> - Monday, Wednesday, Thursday - 6:00am<br/>
                    <strong>Evening</strong> - Tuesday, Friday - 6:30pm
                </p>
            </div>
            <div class="col-md-4">
                <h4>Other Activities</h4>
                <p>
                    <strong>Special Adoration</strong> : Last sunday of every month at 6:00pm
                </p>
                <p>
                    <strong>Infant Baptism</strong> : First Saturday of every month at 6:30am
                </p>
            </div>
            <div class="col-md-4">
                <h4>Registration</h4>
                <p>
                    <strong>Register</strong> as a member of our parish community and receive our weekly bulletin by signing up here
                    <a class="btn btn-warning btn-sm" href="Registration.php" type="button">Sign Up</a>
                </p>
            </div>
        </div>
        <div class="row hometext">
            <div class="col-md-4">
                <h4>About Us</h4>
                <p>We are a Catholic fraternity with our doors open to everyone who needs God</p>
            </div>
            <div class="col-md-4">
                <h4>Contact Us</h4>
                <p>P. O. Box MP 1131, Mamprobi, Accra, Ghana<br/>
                    Tel: +233302316910  <br/>
                    Email: martyrs.mamprobi@gmail.com<br/>
                    Adjacent Shell Filling Station. Opposite Mamprobi Post Office</p>
            </div>
            <div class="col-md-4 ">
                <h4>Parish Priest</h4>
                <p>Very Rev. Fr. Richard Anthony Oppong<br/>
                    tonyrich31@yahoo.com</p>
            </div>
        </div>
        <div class="footerfoot">
            <p>Copyright &copy 2018 Martyrs of Uganda. All rights reserved</p>
        </div>
    </div>
</div>
<script src="components/js/jquery.js" type="text/javascript"></script>
