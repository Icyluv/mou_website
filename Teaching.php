<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="components/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="components/css/stylesheet.css"/>
    <title>Teaching</title>
</head>
<body >
<?php include_once('components/includes/header.php'); ?>

<div class="container-fluid TeachBody">
    <div class="row " >
        <div class="col-md-4">
            <div class="TeachSections" style="background-color:#ffe4e1">
                <h3>From the Vatican</h3></br>
                <div class="row">
                    <div class="col-md-12">
                        <h5>About the early church fathers</h5>
                        <p>
                            The early church fathers, starting from the Apostles began to spread the gospel of salvation throughout the areas
                            of God's chosen people. Until Paul came along and spread the word to the Gentile nations
                        </p>
                    </div>
                </div></br>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Quotes from the Compendium of the Catholic Church</h5>
                        <p>
                            “Sin is a personal act. Moreover, we have a responsibility for the sins committed by others when we cooperate in them:
                            by participating directly and voluntarily in them; by ordering, advising, praising, or approving them;
                            by not disclosing or not hindering them when we have an obligation to do so; by protecting evil-doers. [1868]”
                            ― The Catholic Church, Catechism of the Catholic Church
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4" >
            <div class="TeachSections" style="background-color: #f5f5dc">
                <h3>Quiet Time</h3></br>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Readings for the Week</h5>
                    </div>
                    <p>
                        <strong>Monday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Tuesday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Wednesday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Thursday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Friday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Saturday</strong>: Sir 34:12-14/Ps 25:4-5,6-7,8-9/Lk 4:3-8</br>
                        <strong>Sunday</strong>: Ex 17:3-7/Ps 95:1-2,6-7,8-9/Rom 5:1-2,5-8/Mt 11:12-16
                    </p>
                </div></br>
                <div class="row">
                    <div class="col-md-12">
                        <h5>Reflection for the Week</h5>
                        <p>
                            The temple in Jerusalem was a place of offering sacrifices and seeking the face of Yahweh. For this reason animals for
                            sacrifice were sold at the temple. Again the Jews considered Roman currency as a pagan item because it bore the image of
                            the head of Caesar. There were money changers at the temple who traded the Roman currency for Jewish ones for use in the temple.
                            However, because of the monopoly these traders enjoyed, they cheated their customers. The House of God which was supposed to be
                            a house of liberation now has become a place of cheating and injustice. Likewise, we have allowed the temple of our bodies to be
                            filled with all manner of evil. We have allowed dust to collect under stationary objects we have not moved in ages. We have watered
                            down our values to accommodate evil. Jesus cleanses the temple with a whip. A whip is that which causes pain. This tells us that the
                            process of cleansing ourselves, the process of doing away with the evil we have become used to over time, is a painful yet very
                            necessary step. In our days of catechism and Sunday school, one of the very first things we may have come across is the Decalogue
                            (Ten Commandments). The first reading reminds us of the basic guidelines we are to follow in order to clean our temple.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="TeachSections" style="background-color: #add8e6">
                <h3>Marian Corner</h3>
                <div class="row">
                    <div class="col-md-12">
                        <img class="TeachMarian imgMarian" src="components/images/OurLadyOfLourdes1.jpg" /></br>
                        <h5>Our Lady of Lourdes</h5>
                        <p>
                            In 1858, Bernadette Soubirous reported a vision of Our Lady of Lourdes.A simple 14-year-old peasant girl of no significant educational
                            experience, Soubirous claimed she saw uo petito damizelo, "a small maiden," in white, with a golden rosary and blue belt fastened around her waist,
                            and two golden roses at her feet. In subsequent visitations she heard the lady speak to her, saying "Que soy era Immaculada Concepciou"
                            (I am the Immaculate Conception), and asking that a chapel be built there.
                            At first ridiculed, questioned, and belittled by Church officials and other contemporaries,
                            Soubirous insisted on her vision. Eventually the Church believed her and she was canonized by Pope Pius XI in 1933.

                            After church investigations confirmed her visions, a large church was built at the site. Lourdes is now a major Marian pilgrimage site.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include_once('components/includes/footer.php'); ?>
</body>
</html>