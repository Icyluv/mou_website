<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="components/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="components/css/stylesheet.css"/>
    <title>Registration</title>

</head>
<body>

<?php include_once('components/includes/header.php'); ?>

<div>
    <h5>Membership Registration</h5>
</div>

</br><form>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-2">
            <label for="inputEmail4">Title</label>
            <input type="email" class="form-control" id="inputEmail4" placeholder="Mr/Mrs/Miss/Dr/Prof">
        </div>
        <div class="form-group col-md-4">
            <label for="inputPassword4">First Name</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Given Name">
        </div>
        <div class="form-group col-md-4">
            <label for="inputPassword4">Surname</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Family Name">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-5">
            <label for="inputPassword4">Other Names</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Other Names">
        </div>
        <div class="form-group col-md-5">
            <label for="inputPassword4">Maiden Name</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Previous Name">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-5">
            <label for="inputAddress">Residential Address</label>
            <input type="text" class="form-control" id="inputAddress" placeholder="">
        </div>
        <div class="form-group col-md-5">
            <label for="inputAddress2">Postal Address</label>
            <input type="text" class="form-control" id="inputAddress2" placeholder="P. O. Box">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-4">
            <label for="inputCity">Town</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-4">
            <label for="inputCity">City</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-2">
            <label for="inputState">Region/State</label>
            <input type="text" class="form-control" id="inputState">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-3">
            <label for="inputCity">Country</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-3">
            <label for="inputCity">Telephone Number</label>
            <input type="text" class="form-control" id="inputCity" placeholder="+">
        </div>
        <div class="form-group col-md-3">
            <label for="inputState">Mobile Number</label>
            <input type="text" class="form-control" id="inputState" placeholder="+">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-5">
            <label for="inputCity">Profession</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-5">
            <label for="inputCity">Current Place of Work</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-1"></div>
        <div class="form-group col-md-2">
            <label for="inputCity">Baptismal Card #</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
        <div class="form-group col-md-2">
            <label for="inputCity">Church Dues Card #</label>
            <input type="text" class="form-control" id="inputCity">
        </div>
    </div>
</form>
</br><label class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input">
    <span class="custom-control-indicator"></span>
    <span class="custom-control-description">I want to receive the weekly bulletin</span>
</label>
</br></br><button type="submit" class="btn btn-primary">Submit</button>

</body>
</html>