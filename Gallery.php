<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="components/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="components/css/stylesheet.css"/>
    <title>Gallery</title>
</head>
<body>
<?php include_once('components/includes/header.php'); ?>

<div class="container ">
    <div class="row">
        <div class="col-md-12" id="gallhead">
            <h3 class="redColour c-align" id="gallFont">Martyrs in Print</h3>
        </div>
    </div>
</div>
<div class="container-fluid ">
    <div class="row">
        <div class="col-md-3 r-line leftList">
            <?php
            if ($handle = opendir('components/gallery')) {
                $album1=0;
                while (false !== ($entry = readdir($handle))) {
                    $album1++;
                    if ($entry != "." && $entry != "..") {
                        ?>
                        <div class="row ">
                            <div class="col-md-1"></div>
                            <div class="col-md-10 b-line space20" style="margin-top:25px">
                                <h5><?php echo ucwords($entry); ?></h5>
                            </div>
                            <div class="col-md-1"></div>
                            <article style="display: none;">
                            <div class="row">
                                <?php if ($subhandle = opendir('components/gallery/' . $entry)) {
                                    $album=0;
                                    while (false !== ($subentry = readdir($subhandle))) {
                                        $album++;
                                        if ($subentry != "." && $subentry != "..") { ?>
                                            <div class="col-md-3">
                                                <div class="gallery_img_container gallery_img_container2 " onclick="showInner('album<?php echo $album.$album1;?>')">
                                                <!-- <img src="components/gallery/<?php echo $galyth[$i]; ?>"
                                                     width="100%"
                                                     class="img-thumbnail albumImage"> -->
                                                </div>
                                                <article style="display: none;" id="album<?php echo $album.$album1;?>">
                                                    <div class="row">
                                                        <?php if(is_dir('components/gallery/' . $entry."/".$subentry)){
                                                            $path='components/gallery/' . $entry."/".$subentry;
                                                            if ($subhandle1 = opendir($path)) {
                                                            while (false !== ($subentry1 = readdir($subhandle1))) {
                                                                if ($subentry1 != "." && $subentry1 != "..") {
                                                                    if(file_exists($path.'/'.$subentry1)){ ?>
                                                                    <div class="col-md-3">
                                                                    <div class="">
                                                                        <img src="<?php echo $path.'/'.$subentry1?>"
                                                                            height="100%"
                                                                            class="img-thumbnail"/>
                                                                            </div>
                                                                    </div>
                                                                <?php }
                                                                }
                                                            }
                                                        }
                                                            closedir($subhandle1);
                                                        } ?>
                                                    </div>
                                                </article> 
                                                <h5><?php echo ucwords($subentry); ?></h5>
                                            </div>
                                        <?php }
                                    }
                                    closedir($subhandle);
                                } ?>
                                </div>
                            </article>
                        </div>
                    <?php }
                }
                closedir($handle);
            } ?>

        </div>
        <div class="col-md-9 galleryContainer">
        </div>
    </div>
</div>


<?php include_once('components/includes/footer.php'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        // $(".gallery_img_container").click(function () {
        //     alert("auha");
        //     // var gall = $(this).parent().find("article").html();
            
        //     // $(".galleryContainer").html(gall);
        // });

        $(".leftList h5").click(function () {
            var gall = $(this).parent().parent().find("article").html();

            $(".galleryContainer").html(gall);
        });
        
    });

    function showInner(id){
        var gall = $("#"+id).html();

        $(".galleryContainer").html(gall);
    }
</script>
</body>
</html>