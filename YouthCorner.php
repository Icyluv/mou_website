<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link rel="stylesheet" href="components/css/bootstrap.min.css"/>
    <link type="text/css" rel="stylesheet" href="components/css/stylesheet.css"/>
    <title>YouthCorner</title>
</head>
<body>
<?php  include_once('components/includes/header.php');?>
<div class="c-align">
    <h1>Martyrs Youth</h1>
    <p>Power In Action</p>
</div>
<div class="container">
    <div class="row home_images">
        <div class="col-md-3 imgContainer lwanga">
            <div class="homeimg">
                <img src="components/images/y13.jpeg">
            </div>
            <div>
                <h3>Martyrs Youth</h3>
                <p>
                    Power in Action. We are the future leaders of our great parish and we will contribute in our
                    diverse ways to support the leaders of the parish to build the house of God, a befitting one at
                    that. Martyrs of Uganda Parish will rise again and be sanctuary for all ... who need the peace of
                    Christ
                </p>
            </div>
        </div>
        <div class="col-md-3 imgContainer lwanga">
            <div class="homeimg">
                <img src="components/images/y14.jpeg">
            </div>
            <div>
                <h3>Martyrs Youth</h3>
                <p>
                    Power in Action. We are the future leaders of our great parish and we will contribute in our
                    diverse ways to support the leaders of the parish to build the house of God, a befitting one at
                    that. Martyrs of Uganda Parish will rise again and be sanctuary for all ... who need the peace of
                    Christ
                </p>
            </div>
        </div>
        <div class="col-md-3 imgContainer lwanga">
            <div class="homeimg">
                <img src="components/images/y4.jpeg">
            </div>
            <div>
                <h3>Martyrs Youth</h3>
                <p>
                    Power in Action. We are the future leaders of our great parish and we will contribute in our
                    diverse ways to support the leaders of the parish to build the house of God, a befitting one at
                    that. Martyrs of Uganda Parish will rise again and be sanctuary for all ... who need the peace of
                    Christ
                </p>
            </div>
        </div>
        <div class="col-md-3 imgContainer lwanga">
            <div class="homeimg">
                <img src="components/images/y5.jpeg">
            </div>
            <div>
                <h3>Martyrs Youth</h3>
                <p>
                    Power in Action. We are the future leaders of our great parish and we will contribute in our
                    diverse ways to support the leaders of the parish to build the house of God, a befitting one at
                    that. Martyrs of Uganda Parish will rise again and be sanctuary for all ... who need the peace of
                    Christ
                </p>
            </div>
        </div>
    </div>
</div>


<?php  include_once('components/includes/footer.php');?>
</body>
</html>